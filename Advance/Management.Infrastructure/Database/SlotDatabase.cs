﻿using Management.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Management.Infrastructure.Database
{
    public class SlotDatabase : DbContext
    {
        public DbSet<Slot> Slots { get; set; }

        public SlotDatabase(DbContextOptions<SlotDatabase> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("Slot_Db");
            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        }
    }

    public static class DbExtension
    {
        public static IServiceCollection AddSlotDb(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<SlotDatabase>(options =>
            {
                options.UseNpgsql(configuration.GetConnectionString("SlotDb"));
            });
            return services;
        }
    }
}
