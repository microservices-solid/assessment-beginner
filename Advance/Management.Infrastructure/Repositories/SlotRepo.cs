﻿using Management.Domain.Contracts;
using Management.Domain.Entities;
using Management.Domain.Exceptions;
using Management.Infrastructure.Database;
using Management.Infrastructure.Exceptions;

namespace Management.Infrastructure.Repositories
{
    public class SlotRepo : ISlotRepository
    {
        private readonly SlotDatabase _db;

        public SlotRepo(SlotDatabase db)
        {
            _db = db;
        }

        public async Task Add(Slot slot)
        {
            _db.Slots.Add(slot);
            await _db.SaveChangesAsync();
        }

        //If id provided, return slot
        //If id not provided, return list of slots
        public async Task<List<Slot>> Get(Guid? id)
        {
            if (id == Guid.Empty)
            {
                throw new SlotIdEmptyException();
            }

            if (id == null)
            {
                return _db.Slots.ToList();
            }

            var slot = _db.Slots.SingleOrDefault(x => x.Id == id);

            if (slot == null)
            {
                throw new SlotNotFoundException((Guid)id);
            }

            return new List<Slot> { slot };
        }

        public async Task<Slot?> GetBySlotId(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new SlotIdEmptyException();
            }

            var slot = _db.Slots.SingleOrDefault(x => x.Id == id);

            if (slot == null)
            {
                throw new SlotNotFoundException((Guid)id);
            }

            return slot;
        }

        public async Task<List<Slot>> GetAvailableSlots()
        {
            var slots = _db.Slots.Where(x => !x.IsReserved && x.Time > DateTime.Now.ToUniversalTime());
            return slots.ToList();
        }

        public async Task<List<Slot>> GetByDoctorId(Guid doctorId)
        {
            if (doctorId == Guid.Empty)
            {
                throw new DoctorIdInvalidException();
            }

            var slots = _db.Slots.Where(x => x.DoctorId == doctorId);
            return slots.ToList();
        }

        public async Task<List<Slot>> GetByDoctorName(string name)
        {
            if (name == null || name == "")
            {
                throw new DoctorNameInvalidException();
            }

            var slots = _db.Slots.Where(x => x.DoctorName == name);
            return slots.ToList();
        }

        public async Task<List<Slot>> GetReservedSlotsByDoctorName(string name)
        {
            if (name == null || name == "")
            {
                throw new DoctorNameInvalidException();
            }

            var slots = _db.Slots.Where(x => x.DoctorName == name && x.IsReserved == true);
            return slots.ToList();
        }

        public bool IsSlotExist(Guid id)
        {
            return _db.Slots.Any(x => x.Id == id);
        }

        public bool IsSlotExist(Slot s)
        {
            return _db.Slots.Any(x => x.DoctorName == s.DoctorName && x.Time == s.Time);
        }

        public async Task Update(Slot slot)
        {
            _db.Slots.Update(slot);
            await _db.SaveChangesAsync();
        }

        public async Task SetSlotIsReserved(Guid id, bool isReserved)
        {
            var slot = _db.Slots.SingleOrDefault(x => x.Id == id);

            if (slot == null)
            {
                throw new SlotNotFoundException((Guid)id);
            }

            slot.IsReserved = isReserved;
            await _db.SaveChangesAsync();
        }

        public async Task<List<Slot>> GetReservedSlotsByDoctorId(Guid doctorId)
        {
            if (doctorId == Guid.Empty)
            {
                throw new DoctorIdInvalidException();
            }

            var slots = _db.Slots.Where(x => x.DoctorId == doctorId && x.IsReserved);
            return slots.ToList();
        }
    }
}
