﻿using Convey.MessageBrokers;
using Management.Application.Contracts;

namespace Management.Infrastructure
{
    public class RabbitMQManagementPublisher : IManagementPublisher
    {
        private readonly IBusPublisher _publisher;

        public RabbitMQManagementPublisher(IBusPublisher publisher)
        {
            _publisher = publisher;
        }

        public async Task Publish(SlotAddedEventDto @event)
        {
            await _publisher.PublishAsync(@event);
        }
    }
}
