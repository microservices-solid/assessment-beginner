﻿using Booking.Application.Dtos;
using Booking.Application.UseCases;
using Management.Shared;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Data;
using System.Text.Json;

namespace Booking.API.Controllers
{
    [Route("/appointments")]
    public class AppointmentController : ControllerBase
    {
        private readonly GetAllDoctorsAvailableSlots _getAllDoctorsAvailableSlots;
        private readonly BookAppointment _bookAppointment;
        private readonly GetUpcomingAppointmentsForDoctor _getUpcomingAppointmentsForDoctor;
        private readonly SetAppointmentCompleted _setAppointmentCompleted;
        private readonly DeleteAppointment _deleteAppointment;
        private readonly GetAllAppointments _getAllAppointments;
        //private INotificationModuleAPI _notificationModuleAPI;
        private readonly ILogger<AppointmentController> _logger;
        public AppointmentController(GetAllDoctorsAvailableSlots getAllDoctorsAvailableSlots, BookAppointment bookAppointment, GetUpcomingAppointmentsForDoctor getUpcomingAppointmentsForDoctor, SetAppointmentCompleted setAppointmentCompleted, DeleteAppointment deleteAppointment, GetAllAppointments getAllAppointments, ILogger<AppointmentController> logger)
        {
            _getAllDoctorsAvailableSlots = getAllDoctorsAvailableSlots;
            _bookAppointment = bookAppointment;
            _getUpcomingAppointmentsForDoctor = getUpcomingAppointmentsForDoctor;
            _setAppointmentCompleted = setAppointmentCompleted;
            _deleteAppointment = deleteAppointment;
            _getAllAppointments = getAllAppointments;
            //_notificationModuleAPI = notificationModuleAPI;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var appointments = await _getAllAppointments.Execute();
            return Ok(appointments);
        }

        [HttpPatch("/appointments/{appointmentId}/complete")]
        [Authorize(Roles = "doctor")]
        public async Task<IActionResult> MarkAppointmentComplete(Guid appointmentId)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values
                    .SelectMany(value => value.Errors)
                    .Select(error => error.ErrorMessage)
                    .ToList();
                _logger.LogError(JsonSerializer.Serialize(errors));
                return BadRequest(errors);
            }
            await _setAppointmentCompleted.Execute(appointmentId);
            _logger.LogInformation($"Appointment of id {appointmentId} was marked complete!");
            return Ok("Appointment marked complete!");
        }

        [HttpDelete("/appointments/{appointmentId}")]
        [Authorize(Roles = "doctor")]
        public async Task<IActionResult> DeleteAppointment(Guid appointmentId) {
            await _deleteAppointment.Execute(appointmentId);
            _logger.LogInformation($"Appointment of id {appointmentId} was deleted!");
            return Ok("Appointment cancelled!");
        }

        [HttpGet("/appointments/slots")]
        [Authorize(Roles = "patient")]
        public async Task<IActionResult> GetAvailableSlots()
        {
            var slots = await _getAllDoctorsAvailableSlots.Execute();
            if (slots == null || slots.Count == 0)
            {
                _logger.LogInformation("No available slots found!");
                return BadRequest("No available slots found!");
            }
            _logger.LogInformation($"Available slots: {slots.Count}");
            return Ok(slots);
        }

        [HttpPost("/appointments")]
        [Authorize(Roles = "patient")]
        public async Task<IActionResult> SetAppointmentV1([FromBody] NewAppointmentRequest request)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values
                    .SelectMany(value => value.Errors)
                    .Select(error => error.ErrorMessage)
                    .ToList();
                _logger.LogError(JsonSerializer.Serialize(errors));
                return BadRequest(errors);
            }

            await _bookAppointment.Execute(request);
            _logger.LogInformation($"Appointment booked by patient of id {request.PatientId} for the slot of id {request.SlotId}");

            //Send confirmation notification
            //await _notificationModuleAPI.SendConfirmation(request);

            return Ok("Appointment booked!");
        }

        [HttpGet("/doctor/{doctorId}/appointments")]
        [Authorize(Roles = "doctor")]
        public async Task<IActionResult> GetUpcomingAppointmentsByDoctorV1(Guid doctorId)
        {
            var appointments = await _getUpcomingAppointmentsForDoctor.Execute(doctorId);
            if (appointments == null)
            {
                _logger.LogInformation($"No upcoming appointments for doctor of id {doctorId}");
                return BadRequest("No upcoming appointments!");
            }
            _logger.LogInformation($"Upcoming appointments for doctor of id {doctorId}: {appointments.Count}");
            return Ok(appointments);
        }
    }
}
