﻿using Booking.Shared.Events;
using Convey.MessageBrokers.RabbitMQ;
using Management.Infrastructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Notification.Application;
using Notification.Application.UseCases;

namespace Notification.API
{
    public static class Extensions
    {
        public static IServiceCollection AddNotificationModule(this IServiceCollection services)
        {
            services.AddApplication();
            return services;
        }

        public static IApplicationBuilder UseNotificationModule(this IApplicationBuilder app)
        {
            app.UseRabbitMq().Subscribe<SlotAddedEventDto>(async (serviceProvider, message, context) =>
            {
                await serviceProvider.GetRequiredService<SlotAddedEventHandler>().Handle(message);
            });
            app.UseRabbitMq().Subscribe<NewAppointmentBookedEventDto>(async (serviceProvider, message, context) =>
            {
                await serviceProvider.GetRequiredService<NewAppointmentBookedEventHandler>().Handle(message);
            });
            app.UseRabbitMq().Subscribe<AppointmentCancelledEventDto>(async (serviceProvider, message, context) =>
            {
                await serviceProvider.GetRequiredService<AppointmentCancelledEventHandler>().Handle(message);
            });
            app.UseRabbitMq().Subscribe<AppointmentMarkCompletedEventDto>(async (serviceProvider, message, context) =>
            {
                await serviceProvider.GetRequiredService<AppointmentMarkCompletedEventHandler>().Handle(message);
            });
            return app;
        }
    }
}