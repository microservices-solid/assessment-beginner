# To setup postgres by first pre-creating pv-chain from previous database update when running docker compose
helm install appointmentbooking-postgres bitnami/postgresql --set persistence.existingClaim=postgresql-pv-chain --set volumePermissions.enabled=true

# docker images are to be updated with correct appsettings.Development.Kubernetes and built first
# kubernetes commands to be run in Advance folder where volumes folder are stored
kubectl apply -f Kubernetes\deployment-appointmentbooking.yaml
kubectl delete -f Kubernetes\deployment-appointmentbooking.yaml

# To find out more about the service, pod or deployment
kubectl get all
kubectl describe <type>/<name>
kubectl logs <name>
kubectl get <type>/<name> -o json

# Remember to initialize rabbitmq by adding direct exchange bookings via rabbitmq management portal
kubectl port-forward --namespace default svc/appointmentbooking-rabbitmq 15672:15672

# adminer is running at Nodeport 30382 
