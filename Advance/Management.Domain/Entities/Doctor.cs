﻿using System.ComponentModel.DataAnnotations;

namespace Management.Domain.Entities
{
    public class Doctor
    {
        [Required] public string Name { get; set; }
        [Required] public Guid Id { get; set; }
    }
}
