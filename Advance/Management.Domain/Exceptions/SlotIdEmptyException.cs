﻿namespace Management.Domain.Exceptions
{
    public class SlotIdEmptyException : Exception
    {
        public SlotIdEmptyException() : base("Slot Id should not be empty!")
        {
        }
    }
}
