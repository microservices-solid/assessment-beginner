﻿# Advance Microservices Training

## Description:

Hey there champs! 🚀

Remember that awesome Intermediate Level Assessment? Good news, it’s time to level up!
This time around, you’ll have to showcase your shiny new skills with microservices,
container orchestration, and some deployment magic. Let’s dive in!
Your mission is to refactor the Doctor Appointment Booking System. We're shifting gears
and moving the Notification module to its own microservice. Plus, there's a fun twist
involving RabbitMQ, Docker, and Kubernetes!


## Requirements:

	1. Notification Microservice

		● Extract the Notification module and make it its own .NET Core app. This will
	be our Notification microservice.

		● For now, keep things simple; when a notification event happens, just log the
	details.


	2. Eventing with RabbitMQ

		● Whenever something noteworthy happens in the main app (like scheduling
	an appointment), publish an event to RabbitMQ.

		● Our new Notification microservice? That'll consume these events from
	RabbitMQ and handle the notifications.


	3. Containerization with Docker

		● Wrap up both apps (the main one and the Notification service) in neat little
	Docker containers.


	4. Deployment Fun with Kubernetes and Helm

		● Time for some K8s action! Deploy everything (yes, including RabbitMQ) onto
	a Kubernetes cluster.

		● And remember that Helm magic we talked about in the course? Put it to good
	use here to deploy rabbitMq.


	5. Show & Tell

		● Capture a brief video showing all services deployed and running smoothly on
	Kubernetes. Just a quick tour, nothing fancy!


## Deliverables:

Push all your fantastic updates to your GitHub/Gitlab repo.

	● Remember the mantra: commit early, commit often. Let’s see that journey!

	● That cool video tour of your deployed services


## Evaluation Criteria:

Smooth extraction of the Notification microservice.

	● Correct implementation and interaction with RabbitMQ.

	● Proper containerization of apps.

	● Successful deployment on Kubernetes (Bonus for clean Helm charts!).

	● Video clarity and the presentation of services running.



Alright, wizards, I know you’ve got this! Knock it out of the park and may the code be with
you. 🌟

Good luck!
