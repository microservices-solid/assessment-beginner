﻿using Management.Application.Dtos;
using Management.Application.Services;
using Management.Domain.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using Microsoft.Extensions.Logging;
using System.Data;
using System.Text.Json;

namespace assessment.Controllers
{
    [Route("/slots")]
    public class SlotController : ControllerBase
    {
        private readonly ISlotService _slotService;
        private readonly ILogger<SlotController> _logger;

        public SlotController(ISlotService slotservice, ILogger<SlotController> logger) { 
            _slotService = slotservice;
            _logger = logger;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var slots = await _slotService.GetAllSlots();
            return Ok(slots);

        }

        [HttpGet("/slots/{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var slot= await _slotService.GetBySlotId(id);

            if (slot == null)
            {
                return BadRequest("Slot not found!");
            }
            return Ok(slot);
        }

        [HttpPost("/slots/doctor")]
        public async Task<IActionResult> GetByDoctor([FromBody] Doctor doctor) 
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values
                    .SelectMany(value => value.Errors)
                    .Select(error => error.ErrorMessage)
                    .ToList();
                return BadRequest(errors);
            }

            var slots = await _slotService.GetByDoctorId(doctor.Id);
            
            if (slots == null || slots.Count == 0)
            {
                return BadRequest("Slots not found!");
            }
            return Ok(slots);
        }

        [HttpGet("/doctor/{doctorId}/slots")]
        [Authorize(Roles = "doctor")]
        public async Task<IActionResult> GetByDoctorId(Guid doctorId)
        {
            var slots = await _slotService.GetByDoctorId(doctorId);

            if (slots == null || slots.Count == 0)
            {
                _logger.LogInformation($"Slots not found for doctor of id {doctorId}");
                return BadRequest("Slots not found!");
            }
            _logger.LogInformation($"List my slots requested by doctor of id {doctorId}");            
            return Ok(slots);
        }

        [HttpPost]
        internal async Task<IActionResult> AddSlot([FromBody] NewSlotRequest request)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values
                    .SelectMany(value => value.Errors)
                    .Select(error => error.ErrorMessage)
                    .ToList();
                return BadRequest(errors);
            }

            await _slotService.Add(request);
            return Ok("Slot added");
        }

        [HttpPost("/doctor/{doctorId}/slots")]
        [Authorize(Roles = "doctor")]
        public async Task<IActionResult> AddSlotV1(Guid doctorId, [FromBody] NewSlotRequest request)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values
                    .SelectMany(value => value.Errors)
                    .Select(error => error.ErrorMessage)
                    .ToList();
                _logger.LogError(JsonSerializer.Serialize(errors));
                return BadRequest(errors);
            }

            if (doctorId.CompareTo(request.DoctorId) != 0)
            {
                _logger.LogError($"doctor id of {request.DoctorId} in new slot addition request does not match signed in doctor id of {doctorId}");
                return BadRequest("DoctorId does not match!");
            }

            await _slotService.Add(request);
            _logger.LogInformation($"Slot at {request.Time} added by doctor of id {request.DoctorId}");
            return Ok("Slot added");
        }
    }
}
