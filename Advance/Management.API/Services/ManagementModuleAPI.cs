﻿using Management.Application.Services;
using Management.Domain.Entities;
using Management.Domain.Exceptions;
using Management.Shared;
using System.Xml.Linq;

namespace Management.API.Services
{
    public class ManagementModuleAPI : IManagementModuleAPI
    {
        private readonly ISlotService _slotService;
        public ManagementModuleAPI(ISlotService slotService) { 
            _slotService = slotService;
        }

        public async Task<List<SlotDto>> GetAvailableSlots()
        {
            List<Slot> slots = await _slotService.GetAvailableSlots();
            return SlotListToSlotDtoList(slots);
        }

        private static List<SlotDto> SlotListToSlotDtoList(List<Slot> slots)
        {
            if (slots == null) return null;
            List<SlotDto> slotDtos = new List<SlotDto>();
            foreach (Slot slot in slots)
            {
                slotDtos.Add(new SlotDto(slot.Id, slot.Time, slot.DoctorId, slot.DoctorName, slot.IsReserved, slot.Cost));
            }
            return slotDtos;
        }

        public async Task<SlotDto?> GetBySlotId(Guid id)
        {
            Slot slot = await _slotService.GetBySlotId(id);
            if (slot == null) return null;
            return new SlotDto(slot.Id, slot.Time, slot.DoctorId, slot.DoctorName, slot.IsReserved, slot.Cost);
        }

        public async Task<List<SlotDto>> GetReservedSlotsByDoctorName(string name)
        {
            List<Slot> slots = await _slotService.GetReservedSlotsByDoctorName(name);
            return SlotListToSlotDtoList(slots);
        }

        public async Task SetSlotIsReserved(SlotDto slotDto, bool isReserved)
        {
            await _slotService.SetSlotIsReserved(slotDto.Id, isReserved);
        }

        public async Task<List<SlotDto>> GetReservedSlotsByDoctorId(Guid doctorId)
        {
            List<Slot> slots = await _slotService.GetReservedSlotsByDoctorId(doctorId);
            return SlotListToSlotDtoList(slots);
        }
    }
}
