using Convey;
using Convey.MessageBrokers.RabbitMQ;
using Microsoft.AspNetCore.HttpLogging;
using Notification.API;
using Serilog;

var builder = WebApplication.CreateBuilder(args);
builder.Host.UseSerilog((context, services, configuration) =>
{
    configuration
        .ReadFrom.Configuration(context.Configuration)
        .ReadFrom.Services(services);
});
builder.Services.AddHttpLogging(options =>
{
    options.LoggingFields = HttpLoggingFields.All;
});
builder.Services.AddNotificationModule();
builder.Services.AddConvey().AddRabbitMq();
var app = builder.Build();

app.UseHttpLogging();
app.UseNotificationModule();
app.MapGet("/", () => "Notification!");
app.Logger.LogInformation("Notification Application started!");

app.Run();
