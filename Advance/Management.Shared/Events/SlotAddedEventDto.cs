﻿using Convey.MessageBrokers;

namespace Management.Infrastructure
{
    [Message(exchange: "bookings", routingKey: "bookings.slotAdded", queue: "notifications.slotAdded")]
    public record SlotAddedEventDto (Guid slotId, DateTime time, Guid doctorId, decimal cost);
    
}