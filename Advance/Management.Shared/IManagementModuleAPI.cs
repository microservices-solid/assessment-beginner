﻿namespace Management.Shared
{
    public interface IManagementModuleAPI
    {
        public Task<List<SlotDto>> GetReservedSlotsByDoctorName(string name);
        public Task<SlotDto?> GetBySlotId(Guid id);
        public Task SetSlotIsReserved(SlotDto slotDto, bool isReserved);
        public Task<List<SlotDto>> GetAvailableSlots();
        public Task<List<SlotDto>> GetReservedSlotsByDoctorId(Guid doctorId);
    }

    public record SlotDto (
        Guid Id, 
        DateTime Time,
        Guid DoctorId,
        string DoctorName,
        bool IsReserved,
        decimal Cost
    );

    public record DoctorDto (
        Guid DoctorId,
        string DoctorName
    );

    public record PatientDto ();
}