﻿using System.ComponentModel.DataAnnotations;

namespace Management.Application.Dtos
{
    public class NewSlotRequest
    {
        [Required] public string Time { get; set; }
        [Required] public Guid DoctorId { get; set; }
        [Required] public string DoctorName { get; set; }
        public decimal Cost { get; set; }
    }
}
