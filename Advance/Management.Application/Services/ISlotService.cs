﻿using Management.Domain.Entities;
using Management.Application.Dtos;

namespace Management.Application.Services
{
    public interface ISlotService
    {
        public Task<List<Slot>> GetAllSlots();
        public Task<Slot?> GetBySlotId(Guid id);
        public Task<List<Slot>> GetAvailableSlots();
        public Task<List<Slot>> GetByDoctorId(Guid doctorId);
        public Task<List<Slot>> GetReservedSlotsByDoctorName(string name);
        public Task Add(NewSlotRequest request);
        public Task SetSlotIsReserved(Guid id, bool isReserved);
        public Task<List<Slot>> GetReservedSlotsByDoctorId(Guid doctorId);
    }
}
