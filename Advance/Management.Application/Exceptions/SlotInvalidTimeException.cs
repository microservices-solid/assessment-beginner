﻿namespace Management.Application.Exceptions
{
    public class SlotInvalidTimeException : Exception
    {
        

        public SlotInvalidTimeException() : base("Invalid time entered: Please enter a valid later date and time!")
        {
        }
    }
}