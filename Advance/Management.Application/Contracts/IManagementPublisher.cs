﻿using Management.Infrastructure;

namespace Management.Application.Contracts
{
    public interface IManagementPublisher
    {
        Task Publish(SlotAddedEventDto @event);
    }
}
