﻿using Booking.Shared.Events;
using Microsoft.Extensions.Logging;

namespace Notification.Application.UseCases
{
    public class AppointmentCancelledEventHandler
    {
        private ILogger<AppointmentCancelledEventHandler> _logger;

        public AppointmentCancelledEventHandler(ILogger<AppointmentCancelledEventHandler> logger)
        {
            _logger = logger;
        }

        public Task Handle(AppointmentCancelledEventDto notification)
        {
            _logger.LogInformation($"Appointment {notification.AppointmentId} for slot {notification.SlotId} cancelled!");
            return Task.CompletedTask;
        }
    }
}