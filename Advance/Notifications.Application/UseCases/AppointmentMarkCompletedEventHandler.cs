﻿using Booking.Shared.Events;
using Microsoft.Extensions.Logging;

namespace Notification.Application.UseCases
{
    public class AppointmentMarkCompletedEventHandler
    {
        private ILogger<AppointmentMarkCompletedEventHandler> _logger;

        public AppointmentMarkCompletedEventHandler(ILogger<AppointmentMarkCompletedEventHandler> logger)
        {
            _logger = logger;
        }

        public Task Handle(AppointmentMarkCompletedEventDto notification)
        {
            _logger.LogInformation($"Appointment {notification.AppointmentId} marked completed!");
            return Task.CompletedTask;
        }
    }
}