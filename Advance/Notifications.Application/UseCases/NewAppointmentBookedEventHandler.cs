﻿using Booking.Shared.Events;
using Microsoft.Extensions.Logging;

namespace Notification.Application.UseCases
{
    public class NewAppointmentBookedEventHandler
    {
        private ILogger<NewAppointmentBookedEventHandler> _logger;

        public NewAppointmentBookedEventHandler(ILogger<NewAppointmentBookedEventHandler> logger)
        {
            _logger = logger;
        }

        public Task Handle(NewAppointmentBookedEventDto notification)
        {
            _logger.LogInformation($"Appointment {notification.AppointmentId} at UTC time {notification.AppointmentTime.ToUniversalTime()} for doctor {notification.DoctorId} booked by patient {notification.PatientId}");
            return Task.CompletedTask;
        }
    }
}