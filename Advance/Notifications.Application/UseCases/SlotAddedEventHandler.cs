﻿using Management.Infrastructure;
using Microsoft.Extensions.Logging;

namespace Notification.Application.UseCases
{
    public class SlotAddedEventHandler
    {
        private ILogger<SlotAddedEventHandler> _logger;
        public SlotAddedEventHandler(ILogger<SlotAddedEventHandler> logger) { 
            _logger = logger;
        }

        public Task Handle(SlotAddedEventDto notification)
        {
            _logger.LogInformation($"Slot {notification.slotId} at UTC time {notification.time.ToUniversalTime()} of cost {notification.cost} added by doctor {notification.doctorId}!");
            return Task.CompletedTask;
        }
    }
}