﻿using Booking.Domain.Contracts;
using Booking.Domain.Entities;
using Booking.Domain.Exceptions;
using Booking.Infrastructure.Database;
using Booking.Infrastructure.Exceptions;

namespace Booking.Infrastructure.Repositories
{
    public class AppointmentRepo : IAppointmentRepository
    {
        private readonly AppointmentDatabase _db;

        public AppointmentRepo(AppointmentDatabase db)
        {
            _db = db;
        }

        public async Task Add(Appointment appointment)
        {
            _db.Appointments.Add(appointment);
            await _db.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            var appointment = _db.Appointments.Find(id);
            if (appointment == null)
            {
                throw new AppointmentNotFoundException(id);
            }

            _db.Appointments.Remove(appointment);
            await _db.SaveChangesAsync();
        }

        public async Task<List<Appointment>> Get(Guid? id)
        {
            if (id == Guid.Empty)
            {
                throw new AppointmentIdEmptyException();
            }

            if (id == null)
            {
                return _db.Appointments.ToList();
            }

            var appointment = _db.Appointments.SingleOrDefault(x => x.Id == id);

            if (appointment == null)
            {
                throw new AppointmentNotFoundException((Guid)id);
            }

            return new List<Appointment> { appointment };
        }

        public async Task<Appointment?> GetByAppointmentId(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new AppointmentIdEmptyException();
            }

            var appointment = _db.Appointments.SingleOrDefault(x => x.Id == id);

            if (appointment == null)
            {
                throw new AppointmentNotFoundException((Guid)id);
            }

            return appointment;
        }

        public async Task<Appointment?> GetNotCompletedAppointmentBySlotId(Guid slotId)
        {
            var appointment = _db.Appointments.SingleOrDefault(x => x.SlotId == slotId && x.IsCompleted == false);
            return appointment;
        }

        public async Task<List<Appointment>> GetNotCompletedAppointments()
        {
            var appointments = _db.Appointments.Where(x => !x.IsCompleted);
            return appointments.ToList();
        }

        public bool IsAppointmentExist(Appointment a)
        {
            return _db.Appointments.Any(x => x.SlotId == a.SlotId);
        }

        public async Task SetAppointmentIsCompleted(Guid id, bool isCompleted)
        {
            var appointment = _db.Appointments.SingleOrDefault(x => x.Id == id);

            if (appointment == null)
            {
                throw new AppointmentNotFoundException((Guid)id);
            }

            appointment.SetIsCompleted(isCompleted);
            await _db.SaveChangesAsync();
        }

        public async Task Update(Appointment appointment)
        {
            _db.Appointments.Update(appointment);
            await _db.SaveChangesAsync();
        }
    }
}
