﻿using Booking.Application.Contracts;
using Booking.Shared.Events;
using Convey.MessageBrokers;

namespace Booking.Infrastructure
{
    public class RabbitMQBookingPublisher : IBookingPublisher
    {
        private readonly IBusPublisher _publisher;

        public RabbitMQBookingPublisher (IBusPublisher publisher)
        {
            _publisher = publisher;
        }

        public async Task Publish(NewAppointmentBookedEventDto @event)
        {
            await _publisher.PublishAsync(@event);
        }

        public async Task Publish(AppointmentMarkCompletedEventDto @event)
        {
            await _publisher.PublishAsync(@event);
        }

        public async Task Publish(AppointmentCancelledEventDto @event)
        {
            await _publisher.PublishAsync(@event);
        }
    }
}
