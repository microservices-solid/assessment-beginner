﻿using Booking.Application.Contracts;
using Management.Shared;

namespace Booking.Infrastructure
{
    public class ManagementGateway : IManagementGateway
    {
        private readonly IManagementModuleAPI _managementModuleAPI;

        public ManagementGateway(IManagementModuleAPI managementModuleAPI)
        {
            _managementModuleAPI = managementModuleAPI;
        }

        public async Task<List<SlotDto>> GetAvailableSlots()
        {
            return await _managementModuleAPI.GetAvailableSlots();
        }

        public async Task<SlotDto?> GetBySlotId(Guid id)
        {
            return await _managementModuleAPI.GetBySlotId(id);
        }

        public async Task<List<SlotDto>> GetReservedSlotsByDoctorId(Guid doctorId)
        {
            return await _managementModuleAPI.GetReservedSlotsByDoctorId(doctorId);
        }

        public async Task SetSlotIsReserved(SlotDto slotDto, bool isReserved)
        {
            await _managementModuleAPI.SetSlotIsReserved(slotDto, isReserved);
        }
    }
}
