﻿using Booking.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Booking.Infrastructure.Database
{
    public class AppointmentDatabase : DbContext
    {
        public DbSet<Appointment> Appointments { get; set; }

        public AppointmentDatabase(DbContextOptions<AppointmentDatabase> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("Appointment_Db");
            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        }
    }

    public static class DbExtension
    {
        public static IServiceCollection AddAppointmentDb(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppointmentDatabase>(options =>
            {
                options.UseNpgsql(configuration.GetConnectionString("AppointmentDb"));
            });
            return services;
        }
    }
}
