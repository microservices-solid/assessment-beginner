﻿using System.Runtime.Serialization;

namespace Booking.Infrastructure.Exceptions
{
    [Serializable]
    internal class AppointmentNotFoundException : Exception
    {
        public AppointmentNotFoundException() : base($"Appointment not found!")
        {

        }

        public AppointmentNotFoundException(Guid id) : base($"Appointment with id {id} not found!")
        {

        }
    }
}