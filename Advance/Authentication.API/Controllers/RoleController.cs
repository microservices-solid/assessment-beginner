﻿using Authentication.API.Dtos;
using Authentication.API.Security;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace Authentication.API.Controllers
{
    [Route("/AuthModule")]
    public class RoleController : ControllerBase
    {
        private readonly JwtCreator _jwtCreator;
        private readonly ILogger<RoleController> _logger;

        public RoleController(JwtCreator jwtCreator, ILogger<RoleController> logger)
        {
            _jwtCreator = jwtCreator;
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Get()
        {
            return Ok("Authentication Module");
        }

        [HttpPost("/login")]
        public Task<IActionResult> Post([FromBody] LoginRequest request)
        {
            _logger.LogInformation($"LoginRequest from {request.UserRole} role");
            if (request.UserRole == "admin")
            {
                var token = _jwtCreator.GenerateJsonWebToken("admin");
                var loginResponse = new LoginResponse { token = token };
                return Task.FromResult<IActionResult>(Ok(loginResponse));
            }

            if (request.UserRole == "doctor")
            {
                var token = _jwtCreator.GenerateJsonWebToken("doctor");
                var loginResponse = new LoginResponse { token = token };
                return Task.FromResult<IActionResult>(Ok(loginResponse));
            }

            if (request.UserRole == "patient")
            {
                var token = _jwtCreator.GenerateJsonWebToken("patient");
                var loginResponse = new LoginResponse { token = token };
                return Task.FromResult<IActionResult>(Ok(loginResponse));
            }

            return Task.FromResult<IActionResult>(Unauthorized());
        }
    }
}