﻿using Convey.MessageBrokers;
namespace Booking.Shared.Events
{
    [Message(exchange: "bookings", routingKey: "bookings.appointmentMarkCompleted", queue: "notifications.appointmentMarkCompleted")]
    public record AppointmentMarkCompletedEventDto(Guid AppointmentId);
    
}
