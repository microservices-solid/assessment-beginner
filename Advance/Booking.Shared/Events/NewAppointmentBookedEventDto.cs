﻿using Convey.MessageBrokers;

namespace Booking.Shared.Events
{
    [Message(exchange: "bookings", routingKey: "bookings.newAppointmentBooked", queue: "notifications.newAppointmentBooked")]
    public record NewAppointmentBookedEventDto(Guid AppointmentId, Guid PatientId, Guid DoctorId, DateTime AppointmentTime);
    
}
