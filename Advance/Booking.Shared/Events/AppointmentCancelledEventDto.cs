﻿using Convey.MessageBrokers;

namespace Booking.Shared.Events
{
    [Message(exchange: "bookings", routingKey: "bookings.appointmentCancelled", queue: "notifications.appointmentCancelled")]
    public record AppointmentCancelledEventDto(Guid AppointmentId, Guid SlotId);
    
}
