﻿using System.Runtime.Serialization;

namespace Booking.Domain.Exceptions
{
    [Serializable]
    internal class PatientNameInvalidException : Exception
    {
        public PatientNameInvalidException()
        {
        }

        public PatientNameInvalidException(string? message) : base(message)
        {
        }

        public PatientNameInvalidException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected PatientNameInvalidException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}