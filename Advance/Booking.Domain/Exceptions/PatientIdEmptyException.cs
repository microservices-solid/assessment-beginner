﻿using System.Runtime.Serialization;

namespace Booking.Domain.Exceptions
{
    [Serializable]
    internal class PatientIdEmptyException : Exception
    {
        public PatientIdEmptyException()
        {
        }

        public PatientIdEmptyException(string? message) : base(message)
        {
        }

        public PatientIdEmptyException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected PatientIdEmptyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}