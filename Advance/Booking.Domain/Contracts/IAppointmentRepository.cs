﻿using Booking.Domain.Entities;

namespace Booking.Domain.Contracts
{
    public interface IAppointmentRepository
    {
        public Task<List<Appointment>> Get(Guid? id);
        public Task<Appointment?> GetByAppointmentId(Guid id);
        public Task<List<Appointment>> GetNotCompletedAppointments();
        public Task<Appointment?> GetNotCompletedAppointmentBySlotId(Guid slotId);
        public Task Add(Appointment appointment);
        public Task Delete(Guid id);
        public Task Update(Appointment appointment);
        public bool IsAppointmentExist(Appointment appointment);
        public Task SetAppointmentIsCompleted(Guid id, bool isCompleted);
    }
}
