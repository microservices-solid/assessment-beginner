﻿using Booking.Application.Contracts;
using Booking.Domain.Contracts;
using Booking.Domain.Entities;
using Management.Shared;

namespace Booking.Application.UseCases
{
    public class GetUpcomingAppointmentsForDoctor
    {
        private readonly IAppointmentRepository _appointmentRepository;
        private readonly IManagementGateway _managementGateway;
        public GetUpcomingAppointmentsForDoctor(IAppointmentRepository appointmentRepository, IManagementGateway managementGateway) {
            _appointmentRepository = appointmentRepository;
            _managementGateway = managementGateway;
        }

        public async Task<List<Appointment>> Execute(Guid doctorId)
        {
            var slots = await _managementGateway.GetReservedSlotsByDoctorId(doctorId);
            slots = slots.FindAll(x => x.Time > DateTime.Now.ToUniversalTime());
            if (slots.Count == 0)
            {
                return null;
            }

            List<Appointment> appointments = new List<Appointment>();
            foreach (SlotDto slot in slots)
            {
                var a = await _appointmentRepository.GetNotCompletedAppointmentBySlotId(slot.Id);
                if (a != null)
                {
                    appointments.Add(a);
                }
            }

            if (appointments.Count == 0)
            {
                return null;
            }

            return appointments;
        }
    }
}
