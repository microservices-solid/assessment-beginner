﻿using Booking.Application.Contracts;
using Booking.Application.Exceptions;
using Booking.Domain.Contracts;
using Booking.Shared.Events;

namespace Booking.Application.UseCases
{
    public class SetAppointmentCompleted
    {
        private IAppointmentRepository _appointmentRepository;
        private IBookingPublisher _bookingPublisher;
        public SetAppointmentCompleted(IAppointmentRepository appointmentRepository, IBookingPublisher bookingPublisher) { 
            _appointmentRepository = appointmentRepository;
            _bookingPublisher = bookingPublisher;
        }

        public async Task Execute(Guid id)
        {
            //Check if appointment exist
            var appointment = await _appointmentRepository.GetByAppointmentId(id);
            if (appointment == null)
            {
                throw new AppointmentNotFoundException();
            }

            await _appointmentRepository.SetAppointmentIsCompleted(id, true);
            await _bookingPublisher.Publish(new AppointmentMarkCompletedEventDto(appointment.Id));
        }
    }
}
