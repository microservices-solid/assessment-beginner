﻿using Booking.Application.Contracts;
using Management.Shared;

namespace Booking.Application.UseCases
{
    public class GetAllDoctorsAvailableSlots
    {
        private readonly IManagementGateway _managementGateway;

        public GetAllDoctorsAvailableSlots(IManagementGateway managementGateway)
        {
            _managementGateway = managementGateway;
        }

        public async Task<List<SlotDto>> Execute()
        {
            return await _managementGateway.GetAvailableSlots();
        }
    }
}
