﻿using Booking.Application.Contracts;
using Booking.Application.Dtos;
using Booking.Application.Exceptions;
using Booking.Domain.Contracts;
using Booking.Domain.Entities;
using Booking.Shared.Events;

namespace Booking.Application.UseCases
{
    public class BookAppointment
    {
        private readonly IManagementGateway _managementGateway;
        private readonly IAppointmentRepository _appointmentRepository;
        private readonly IBookingPublisher _bookingPublisher;
        public BookAppointment(IAppointmentRepository appointmentRepository, IManagementGateway managementGateway, IBookingPublisher bookingPublisher) { 
            _appointmentRepository = appointmentRepository;
            _managementGateway = managementGateway;
            _bookingPublisher = bookingPublisher;
        }

        public async Task Execute(NewAppointmentRequest request)
        {
            //Check if slot exist
            var slot = await _managementGateway.GetBySlotId(request.SlotId);
            if (slot == null)
            {
                throw new SlotNotFoundException(request.SlotId);
            }

            //Check if slot DateTime already expired
            if (DateTime.Now.ToUniversalTime() > slot.Time)
            {
                throw new SlotTimeExpiredException();
            }

            Appointment appointment = Appointment.CreateNew(new SlotId(request.SlotId), new PatientId (request.PatientId), new PatientName (request.PatientName));
            await _appointmentRepository.Add(appointment);
            await _bookingPublisher.Publish(new NewAppointmentBookedEventDto(appointment.Id, appointment.PatientId, slot.DoctorId, slot.Time));

            //Need to update isReserved property of Slot to reserved before returning Ok
            await _managementGateway.SetSlotIsReserved(slot, true);
        }
    }
}
