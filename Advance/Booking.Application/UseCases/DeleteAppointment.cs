﻿using Booking.Application.Contracts;
using Booking.Application.Exceptions;
using Booking.Domain.Contracts;
using Booking.Shared.Events;

namespace Booking.Application.UseCases
{
    public class DeleteAppointment
    {
        private IAppointmentRepository _appointmentRepository;
        private IManagementGateway _managementGateway;
        private IBookingPublisher _bookingPublisher;

        public DeleteAppointment(IAppointmentRepository appointmentRepository, IManagementGateway managementGateway, IBookingPublisher bookingPublisher)
        {
            _appointmentRepository = appointmentRepository;
            _managementGateway = managementGateway;
            _bookingPublisher = bookingPublisher;
        }

        public async Task Execute(Guid id)
        {
            //Check if appointment exist
            var appointment = await _appointmentRepository.GetByAppointmentId(id);
            if (appointment == null)
            {
                throw new AppointmentNotFoundException();
            }

            //Check current datetime with appointment datetime
            //if passed appointment time, delete the appointment
            //if not passed appointement time, delete appointment and mark slot as not reserved                
            var slot = await _managementGateway.GetBySlotId(appointment.SlotId);
            if (slot == null)
            {
                throw new SlotNotFoundException();
            }

            await _appointmentRepository.Delete(id);
            await _bookingPublisher.Publish(new AppointmentCancelledEventDto(appointment.Id, appointment.SlotId));

            if (DateTime.Now.ToUniversalTime() < slot.Time)
            {
                await _managementGateway.SetSlotIsReserved(slot, false);
            }
        }
    }
}
