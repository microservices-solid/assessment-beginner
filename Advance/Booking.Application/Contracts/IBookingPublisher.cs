﻿using Booking.Application.Dtos;
using Booking.Shared.Events;

namespace Booking.Application.Contracts
{
    public interface IBookingPublisher
    {
        Task Publish(NewAppointmentBookedEventDto @event);
        Task Publish(AppointmentMarkCompletedEventDto @event);
        Task Publish(AppointmentCancelledEventDto @event);
    }
}
