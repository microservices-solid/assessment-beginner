﻿using Management.Shared;

namespace Booking.Application.Contracts
{
    public interface IManagementGateway
    {
        public Task<List<SlotDto>> GetAvailableSlots();
        public Task<SlotDto?> GetBySlotId(Guid id);
        public Task<List<SlotDto>> GetReservedSlotsByDoctorId(Guid doctorId);
        public Task SetSlotIsReserved(SlotDto slotDto, bool isReserved);
    }
}
