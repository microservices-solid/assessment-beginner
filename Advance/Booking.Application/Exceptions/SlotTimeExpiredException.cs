﻿using System.Runtime.Serialization;

namespace Booking.Application.Exceptions
{
    [Serializable]
    internal class SlotTimeExpiredException : Exception
    {
        public SlotTimeExpiredException() : base("Slot expired: Please choose another slot!")
        {
        }
    }
}