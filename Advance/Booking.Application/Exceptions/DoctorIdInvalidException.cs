﻿using System.Runtime.Serialization;

namespace Booking.Application.Exceptions
{
    [Serializable]
    internal class DoctorIdInvalidException : Exception
    {
        public DoctorIdInvalidException() : base($"Doctor id should not be empty!")
        {
        }
    }
}