﻿URL:[gitlab.com/microservices-solid/assessment](http://gitlab.com/microservices-solid/assessment)

Comments:

1. API design is not logically correct
        
    **Create slot** (implemented after commit a44ac46b567e6f715cb52441820e0ffc6f796648)

    `POST slots/doctor` —>  `doctor/{doctorId}/slots`
    
    **Get doctor available slots**
    
    `GET /appointments/slots` —> `GET /doctor/{doctorId}/slots?reserved=false`
    
    **Cancel and complete an appointment** (implemented after commit a44ac46b567e6f715cb52441820e0ffc6f796648)
    
    `POST /appointments/` —> `Patch /appointments/cancel` `Patch /appointments/complete`
    
    **Book new appointment** (implemented after commit a44ac46b567e6f715cb52441820e0ffc6f796648)
    
    `POST /appointments/slots` —> `POST /appointments/`
    
    **GetUpcomingAppointmentsByDoctor** (implemented after commit a44ac46b567e6f715cb52441820e0ffc6f796648)
    
    `POST /appointments/doctors` —> `GET /doctors/{doctorId}?time=2023-02-10`
    
2. Business logic scattered in the controllers, it should be pushed down to the services (implemented after commit c107f7a735855d846d795396167d5ad42cf8b9f8)
3. `_slotService.Get` and `_appointmentService.Get` both return array and they are used to get slot By Id and appointment By Id, it’s better to use explicit `_appointmentService.GetById` that returns single appointment and the same for the slot (implemented after commit 0304b5ebaf6304452009d1a8fcf884dbafb75ac5)
4. `GetUpcomingAppointmentsByDoctor` it was easier to get all appointments by doctor id directly instead of getting all slots and then get all appointments by slotIds (had to do it this way because appointment does not contain doctor info, so had to go through slots then search in appointment tables, search by doctorID is implemented after commit 0304b5ebaf6304452009d1a8fcf884dbafb75ac5)
5. No validation for adding slot that clashes with an existing slot (verified logic already implemented)