﻿using System.Runtime.Serialization;

namespace Beginner.Exceptions
{
    [Serializable]
    internal class AppointmentIdEmptyException : Exception
    {
        public AppointmentIdEmptyException() : base("Appointment Id should not be empty!")
        {
        }
    }
}