﻿using System.Runtime.Serialization;

namespace Beginner.Exceptions
{
    [Serializable]
    internal class DoctorNameInvalidException : Exception
    {
        public DoctorNameInvalidException() : base("Doctor name is invalid!")
        {
        }
    }
}