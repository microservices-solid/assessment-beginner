﻿namespace Beginner.Exceptions
{
    public class DoctorIdInvalidException : Exception
    {
        public DoctorIdInvalidException() : base("Doctor Id is invalid!")
        {
        }
    }
}
