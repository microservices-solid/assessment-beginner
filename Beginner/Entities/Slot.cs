﻿using Microsoft.Extensions.Hosting;
using System.ComponentModel.DataAnnotations;

namespace Beginner.Entities
{
    public class Slot
    {
        //Id should be unique
        [Required] public Guid Id { get; set; }
        //For same Doctor, Time hh:mm tt should be unique
        [Required] public DateTime Time { get; set; }
        [Required] public Guid DoctorId { get; set; }
        [Required] public string DoctorName { get; set; }
        public bool IsReserved { get; set; }
        public decimal Cost { get; set; }
    }
}
