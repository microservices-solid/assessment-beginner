﻿using System.ComponentModel.DataAnnotations;

namespace Beginner.Entities
{
    public class Appointment
    {
        //Id should be unique
        [Required] public Guid Id { get; set; }
        //SlotId should be unique
        [Required] public Guid SlotId { get; set; }
        [Required] public Guid PatientId { get; set; }
        [Required] public string PatientName { get; set; }
        [Required] public DateTime ReservedAt { get; set; }
        public bool IsCompleted { get; set; }
    }
}
