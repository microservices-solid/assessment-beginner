﻿# Beginner Microservices Training

## Business Requirements:

Your application should adhere to the following business requirements:

## Doctor Availability:

- As a doctor, I want to be able to list my slots and add new slot
- As a doctor, I want to be able to add new slot
- A single time slot should have the following:
    - Id: Guid
    - Time: Date → 22/02/2023 04:30 pm
    - DoctorId: Guid
    - DoctorName: string
    - IsReserved: bool
    - Cost: Decimal

### API

- As a doctor, I want to be able to list my slots:

```powershell
### Get all time slots for doctor with {doctorName}
POST /slots/doctor
Content-Type: application/json

{
  "name": "InMemDoc1"
}
```

- As a doctor, I want to be able to add new slot:

```powershell
### Add time slot for doctor with {doctorName}
POST /slots
Content-Type: application/json

{
  "time": "2023-06-20T15:28",
  "doctorId": "2f0e9dde-8b4d-4d07-b605-1b6bec77d316",
  "doctorName": "InMemDoc1",
  "cost": 150
}
```

## Appointment Booking:

- As a patient, I want to be able to view all doctors' available (only) slots
- As a patient, I want to be able to book an appointment on a free slot
- An Appointment should have the following:
    - Id: Guid
    - SlotId: Guid
    - PatientId: Guid
    - PatientName: string
    - ReservedAt: Date

### API

- As a patient, I want to be able to view all doctors' available (only) slots:

```powershell
### Get all available time slots for any doctor
GET /appointments/slots
```

- As a patient, I want to be able to book an appointment on a free slot:

```powershell
### Create an appointment
POST /appointments/slots
Content-Type: application/json

{
  "slotId": "ed11f995-66ec-4a2a-a5dd-e962a9c969b3",
  "patientId": "c45aca88-c500-45a5-81c9-811472cd938e",
  "patientName": "InMemPatient1"
}
```

## Appointment Management: (optional)

- As a doctor, I want to be able to view my upcoming appointments.
- As a doctor, I want to be able to mark appointments as completed or cancel them if necessary.

### API

- As a doctor, I want to be able to view my upcoming appointments:

```powershell
### Get upcoming appointments 
POST /appointments/doctors
Content-Type application/json

{
  "name": "InMemDoc1"
}
```

- As a doctor, I want to be able to mark appointments as completed or cancel them if necessary:

```powershell
### Update appointment data, in this case, would like to update its 'status' property
### Available status codes: 1 (Completed) and -1 (Cancelled)
POST /appointments
Content-Type application/json

{
  "appointmentId": "dce50060-7fa5-4bbb-bb33-8d52c05772e0",
  "actionType": -1
}
```

## Data Persistence:

- Use Entity Framework Core (EF Core) as the data access technology to interact
with the database.
- Design and implement the necessary models and database context to store
appointment details and any additional required information.

## Deliverables:

Push the complete source code of your .NET Core application to a source code repository of your
choice (e.g., GitHub, GitLab, Bitbucket).
Ensure that your commits are done incrementally and reflect your progress throughout the
development process.
Make regular and meaningful commitments to showcase your implementation approach and
demonstrate how you have broken down the project into smaller steps.


## Evaluation Criteria:

Your project will be evaluated based on the following criteria:
1. Correct implementation of all the required business requirements.
2. Proper use of relevant technologies, such as REST APIs and EF Core.
3. Code quality, including readability, maintainability, and adherence to best practices.
4. Effective implementation of the business logic and error handling.