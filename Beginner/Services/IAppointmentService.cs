﻿using Beginner.Entities;

namespace Beginner.Services
{
    public interface IAppointmentService
    {
        public Task<List<Appointment>> Get(Guid? id);
        public Task<List<Appointment>> GetNotCompletedAppointments();
        public Task<List<Appointment>> GetNotCompletedAppointmentsBySlotId(List<Slot> slot);
        public Task Add(Appointment appointment);
        public Task Delete(Guid id);
        public Task Update(Appointment appointment);
    }
}
