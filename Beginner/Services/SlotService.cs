﻿using Beginner.Entities;
using Beginner.Exceptions;
using Beginner.Repositories;
using Beginner.Services.Exceptions;

namespace Beginner.Services
{
    public class SlotService : ISlotService
    {
        private readonly ISlotRepository _slotRepository;

        public SlotService(ISlotRepository slotRepository)
        {
            _slotRepository = slotRepository;
        }

        //Only doctors can add
        public async Task Add(Slot slot)
        {
            var exist = _slotRepository.IsSlotExist(slot);
            if (exist)
            {
                throw new SlotExistException(slot.Id);
            }

            await _slotRepository.Add(slot);
        }

        public async Task<List<Slot>> Get(Guid? id)
        {
            return await _slotRepository.Get(id);
        }

        public async Task Update(Slot slot)
        {
            await _slotRepository.Update(slot);
        }

        public async Task<List<Slot>> GetByDoctorId(Guid doctorId)
        {
            if (doctorId == Guid.Empty)
            {
                throw new DoctorIdInvalidException();
            }
            return await _slotRepository.GetByDoctorId(doctorId);
        }

        public async Task<List<Slot>> GetByDoctorName(string name)
        {
            if (name == null || name == "")
            {
                throw new DoctorNameInvalidException();
            }
            return await _slotRepository.GetByDoctorName(name);
        }

        public async Task<List<Slot>> GetAvailableSlots()
        {
            return await _slotRepository.GetAvailableSlots();
        }

        public async Task<List<Slot>> GetReservedSlotsByDoctorName(string name)
        {
            if (name == null || name == "")
            {
                throw new DoctorNameInvalidException();
            }
            return await _slotRepository.GetReservedSlotsByDoctorName(name);
        }
    }
}
