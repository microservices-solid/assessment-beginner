﻿namespace Beginner.Services.Exceptions
{
    public class SlotExistException : Exception
    {
        public SlotExistException(Guid id) : base($"Slot id {id} already exist!")
        {
        }
    }
}
