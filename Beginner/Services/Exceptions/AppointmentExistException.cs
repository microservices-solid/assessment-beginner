﻿using System.Runtime.Serialization;

namespace Beginner.Services.Exceptions
{
    [Serializable]
    internal class AppointmentExistException : Exception
    {
        public AppointmentExistException(Guid id) : base($"Appointment id {id} already exist!")
        {
        }
    }
}