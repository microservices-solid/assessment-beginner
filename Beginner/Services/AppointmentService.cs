﻿using Beginner.Entities;
using Beginner.Repositories;
using Beginner.Repositories.Exceptions;
using Beginner.Services.Exceptions;

namespace Beginner.Services
{
    public class AppointmentService : IAppointmentService
    {
        private readonly IAppointmentRepository _appointmentRepository;

        public AppointmentService(IAppointmentRepository appointmentRepository)
        {
            _appointmentRepository = appointmentRepository;
        }

        public async Task Add(Appointment appointment)
        {
            var exist = _appointmentRepository.IsAppointmentExist(appointment);
            if (exist)
            {
                throw new AppointmentExistException(appointment.Id);
            }

            await _appointmentRepository.Add(appointment);
        }

        public async Task Delete(Guid Id)
        {
            await _appointmentRepository.Delete(Id);
        }

        public async Task<List<Appointment>> Get(Guid? id)
        {
            return await _appointmentRepository.Get(id);
        }

        public async Task<List<Appointment>> GetNotCompletedAppointments()
        {
            return await _appointmentRepository.GetNotCompletedAppointments();
        }

        public async Task<List<Appointment>> GetNotCompletedAppointmentsBySlotId(List<Slot> slots)
        {
            if (slots == null || slots.Count == 0)
            {
                throw new ArgumentNullException(nameof(slots));
            }

            List<Appointment> appointments = new List<Appointment>();
            foreach (Slot slot in slots)
            {
                var a = await _appointmentRepository.GetNotCompletedAppointmentBySlotId(slot.Id);
                if (a != null)
                {
                    appointments.Add(a);
                }
            }

            if (appointments.Count == 0)
            {
                throw new AppointmentNotFoundException();
            }

            return appointments;
        }

        public async Task Update(Appointment appointment)
        {
            await _appointmentRepository.Update(appointment);
        }
    }
}
