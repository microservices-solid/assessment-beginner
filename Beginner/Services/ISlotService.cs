﻿using Beginner.Entities;

namespace Beginner.Services
{
    public interface ISlotService
    {
        public Task<List<Slot>> Get(Guid? id);
        public Task<List<Slot>> GetAvailableSlots();
        public Task<List<Slot>> GetByDoctorId(Guid doctorId);
        public Task<List<Slot>> GetByDoctorName(string name);
        public Task<List<Slot>> GetReservedSlotsByDoctorName(string name);
        public Task Add(Slot slot);
        public Task Update(Slot slot);
    }
}
