﻿using Beginner.Controllers.Dtos;
using Beginner.Entities;
using Beginner.Services;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace Beginner.Controllers
{
    [Route("/slots")]
    public class SlotController : ControllerBase
    {
        private readonly ISlotService _slotService;
        public SlotController(ISlotService slotservice)
        {
            _slotService = slotservice;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var slots = await _slotService.Get(null);
            return Ok(slots);

        }

        [HttpPost("/slots/{id}")]
        public async Task<IActionResult> GetById(Guid id)
        {
            var slots = await _slotService.Get(id);

            if (slots == null || slots.Count == 0)
            {
                return BadRequest("Slot not found!");
            }
            return Ok(slots);
        }

        [HttpPost("/slots/doctor")]
        public async Task<IActionResult> GetByDoctor([FromBody] Doctor doctor)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values
                    .SelectMany(value => value.Errors)
                    .Select(error => error.ErrorMessage)
                    .ToList();
                return BadRequest(errors);
            }

            var slots = await _slotService.GetByDoctorName(doctor.name);

            if (slots == null || slots.Count == 0)
            {
                return BadRequest("Slots not found!");
            }
            return Ok(slots);
        }


        [HttpPost]
        public async Task<IActionResult> AddSlot([FromBody] NewSlotRequest request)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values
                    .SelectMany(value => value.Errors)
                    .Select(error => error.ErrorMessage)
                    .ToList();
                return BadRequest(errors);
            }

            //Reject request if request time is earlier than current time
            if (DateTime.Parse(request.Time) < DateTime.Now)
            {
                return BadRequest("Invalid time entered: Please enter a valid later date and time!");
            }

            Slot slot = new Slot { Id = Guid.NewGuid(), Time = DateTime.Parse(request.Time).ToUniversalTime(), DoctorId = request.DoctorId, DoctorName = request.DoctorName, Cost = request.Cost, IsReserved = false };
            await _slotService.Add(slot);
            return Ok("Slot added");
        }
    }
}
