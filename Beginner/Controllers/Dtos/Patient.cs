﻿namespace Beginner.Controllers.Dtos
{
    public class Patient
    {
        public string name { get; set; }
        public Guid? id { get; set; }
    }
}
