﻿namespace Beginner.Controllers.Dtos
{
    public class Doctor
    {
        public string name { get; set; }
        public Guid? id { get; set; }
    }
}
