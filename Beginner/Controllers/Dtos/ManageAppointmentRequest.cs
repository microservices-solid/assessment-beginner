﻿using System.ComponentModel.DataAnnotations;

namespace Beginner.Controllers.Dtos
{
    public class ManageAppointmentRequest
    {
        public readonly static int ACTION_MARK_COMPLETE = 1;
        public readonly static int ACTION_CANCEL = -1;
        [Required] public Guid AppointmentId { get; set; }
        [Required] public int ActionType { get; set; }

    }
}
