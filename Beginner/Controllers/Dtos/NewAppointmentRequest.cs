﻿using System.ComponentModel.DataAnnotations;

namespace Beginner.Controllers.Dtos
{
    public class NewAppointmentRequest
    {
        [Required] public Guid SlotId { get; set; }
        [Required] public Guid PatientId { get; set; }
        [Required] public string PatientName { get; set; }

    }
}
