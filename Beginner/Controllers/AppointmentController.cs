﻿using Beginner.Controllers.Dtos;
using Beginner.Entities;
using Beginner.Services;
using Microsoft.AspNetCore.Mvc;

namespace Beginner.Controllers
{
    [Route("/appointments")]
    public class AppointmentController : ControllerBase
    {
        private readonly IAppointmentService _appointmentService;
        private readonly ISlotService _slotService;
        public AppointmentController(IAppointmentService appointmentService, ISlotService slotService)
        {
            _appointmentService = appointmentService;
            _slotService = slotService;
        }

        [HttpGet]
        public async Task<IActionResult> Get()
        {
            var appointments = await _appointmentService.Get(null);
            return Ok(appointments);
        }

        [HttpPost]
        public async Task<IActionResult> Manage([FromBody] ManageAppointmentRequest request)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values
                    .SelectMany(value => value.Errors)
                    .Select(error => error.ErrorMessage)
                    .ToList();
                return BadRequest(errors);
            }

            //Check if appointment exist
            var appointments = await _appointmentService.Get(request.AppointmentId);
            if (appointments == null || appointments.Count == 0)
            {
                return BadRequest("Appointment not found!");
            }

            if (request.ActionType == ManageAppointmentRequest.ACTION_MARK_COMPLETE)
            {
                appointments[0].IsCompleted = true;
                await _appointmentService.Update(appointments[0]);
                return Ok("Appointment marked complete!");
            }

            if (request.ActionType == ManageAppointmentRequest.ACTION_CANCEL)
            {
                //Check current datetime with appointment datetime
                //if passed appointment time, delete the appointment
                //if not passed appointement time, delete appointment and mark slot as not reserved                
                var slots = await _slotService.Get(appointments[0].SlotId);
                if (slots == null || slots.Count == 0)
                {
                    return BadRequest("Invalid appointment: Slot not found!");
                }

                await _appointmentService.Delete(appointments[0].Id);

                if (DateTime.Now.ToUniversalTime() < slots[0].Time)
                {
                    slots[0].IsReserved = false;
                    await _slotService.Update(slots[0]);
                }

                return Ok("Appointment cancelled!");
            }

            return BadRequest("Invalid action on appointment");
        }

        [HttpGet("/appointments/slots")]
        public async Task<IActionResult> GetAvailableSlots()
        {
            var slots = await _slotService.GetAvailableSlots();
            if (slots == null || slots.Count == 0)
            {
                return BadRequest("No available slots found!");
            }
            return Ok(slots);
        }

        [HttpPost("/appointments/slots")]
        public async Task<IActionResult> SetAppointment([FromBody] NewAppointmentRequest request)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values
                    .SelectMany(value => value.Errors)
                    .Select(error => error.ErrorMessage)
                    .ToList();
                return BadRequest(errors);
            }

            //Check if slot exist
            var slots = await _slotService.Get(request.SlotId);
            if (slots == null || slots.Count == 0)
            {
                return BadRequest("Slot not found!");
            }

            //Check if slot DateTime already expired
            if (DateTime.Now.ToUniversalTime() > slots[0].Time)
            {
                return BadRequest("Slot expired: Please choose another slot!");
            }

            Appointment appointment = new Appointment { Id = Guid.NewGuid(), SlotId = request.SlotId, PatientId = request.PatientId, PatientName = request.PatientName, ReservedAt = DateTime.Now.ToUniversalTime(), IsCompleted = false };
            await _appointmentService.Add(appointment);

            //Need to update isReserved property of Slot to reserved before returning Ok
            slots[0].IsReserved = true;
            await _slotService.Update(slots[0]);
            return Ok("Appointment booked!");
        }

        [HttpPost("/appointments/doctors")]
        public async Task<IActionResult> GetUpcomingAppointmentsByDoctor([FromBody] Doctor doctor)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values
                    .SelectMany(value => value.Errors)
                    .Select(error => error.ErrorMessage)
                    .ToList();
                return BadRequest(errors);
            }

            var slots = await _slotService.GetReservedSlotsByDoctorName(doctor.name);
            slots = slots.FindAll(x => x.Time > DateTime.Now.ToUniversalTime());
            if (slots.Count == 0)
            {
                return BadRequest("No upcoming slot reservations found!");
            }

            var appointments = await _appointmentService.GetNotCompletedAppointmentsBySlotId(slots);
            if (appointments == null || appointments.Count == 0)
            {
                return BadRequest("No upcoming appointments found!");
            }

            return Ok(appointments);
        }
    }
}
