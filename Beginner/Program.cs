using Beginner.Database;
using Beginner.Repositories;
using Beginner.Services;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddApplicationDb(builder.Configuration);
//builder.Services.AddTransient<ISlotRepository, InMemorySlotRepo>();
builder.Services.AddTransient<ISlotRepository, SlotRepo>();
builder.Services.AddTransient<ISlotService, SlotService>();

//builder.Services.AddTransient<IAppointmentRepository, InMemoryAppointmentRepo>();
builder.Services.AddTransient<IAppointmentRepository, AppointmentRepo>();
builder.Services.AddTransient<IAppointmentService, AppointmentService>();

builder.Services.AddControllers();
var app = builder.Build();

app.MapGet("/", () => "Assessment Beginner Module");
app.MapControllers();
app.Run();
