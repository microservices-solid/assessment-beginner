﻿using System.Runtime.Serialization;

namespace Beginner.Repositories.Exceptions
{
    [Serializable]
    internal class SlotWithDoctorIdNotFoundException : Exception
    {
        public SlotWithDoctorIdNotFoundException(Guid doctorId) : base($"Slot with doctor id {doctorId} not found!")
        {
        }
    }
}