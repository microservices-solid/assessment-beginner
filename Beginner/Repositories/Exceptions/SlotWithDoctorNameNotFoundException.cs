﻿using System.Runtime.Serialization;

namespace Beginner.Repositories.Exceptions
{
    [Serializable]
    internal class SlotWithDoctorNameNotFoundException : Exception
    {
        public SlotWithDoctorNameNotFoundException(string name) : base($"Slot with doctor name {name} not found!")
        {
        }
    }
}