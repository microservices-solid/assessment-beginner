﻿using System.Runtime.Serialization;

namespace Beginner.Repositories.Exceptions
{
    [Serializable]
    internal class ReservedSlotsWithDoctorNameNotFoundException : Exception
    {
        public ReservedSlotsWithDoctorNameNotFoundException(string name) : base($"Reserved slots with doctor name {name} not found!")
        {
        }
    }
}