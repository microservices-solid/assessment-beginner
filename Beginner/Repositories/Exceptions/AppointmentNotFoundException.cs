﻿using System.Runtime.Serialization;

namespace Beginner.Repositories.Exceptions
{
    [Serializable]
    internal class AppointmentNotFoundException : Exception
    {
        public AppointmentNotFoundException() : base($"Appointment not found!")
        {

        }

        public AppointmentNotFoundException(Guid id) : base($"Appointment with id {id} not found!")
        {

        }
    }
}