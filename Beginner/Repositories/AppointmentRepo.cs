﻿using Beginner.Database;
using Beginner.Entities;
using Beginner.Exceptions;
using Beginner.Repositories.Exceptions;

namespace Beginner.Repositories
{
    public class AppointmentRepo : IAppointmentRepository
    {
        private readonly ApplicationDatabase _db;

        public AppointmentRepo(ApplicationDatabase db)
        {
            _db = db;
        }

        public async Task Add(Appointment appointment)
        {
            _db.Appointments.Add(appointment);
            await _db.SaveChangesAsync();
        }

        public async Task Delete(Guid id)
        {
            var appointment = _db.Appointments.Find(id);
            if (appointment == null)
            {
                throw new AppointmentNotFoundException(id);
            }

            _db.Appointments.Remove(appointment);
            await _db.SaveChangesAsync();
        }

        public async Task<List<Appointment>> Get(Guid? id)
        {
            if (id == Guid.Empty)
            {
                throw new AppointmentIdEmptyException();
            }

            if (id == null)
            {
                return _db.Appointments.ToList();
            }

            var appointment = _db.Appointments.SingleOrDefault(x => x.Id == id);

            if (appointment == null)
            {
                throw new AppointmentNotFoundException((Guid)id);
            }

            return new List<Appointment> { appointment };
        }

        public async Task<Appointment> GetNotCompletedAppointmentBySlotId(Guid slotId)
        {
            var appointment = _db.Appointments.SingleOrDefault(x => x.SlotId == slotId && x.IsCompleted == false);

            if (appointment == null)
            {
                throw new AppointmentNotFoundException();
            }

            return appointment;
        }

        public async Task<List<Appointment>> GetNotCompletedAppointments()
        {
            var appointments = _db.Appointments.Where(x => !x.IsCompleted);
            return appointments.ToList();
        }

        public bool IsAppointmentExist(Appointment a)
        {
            return _db.Appointments.Any(x => x.SlotId == a.SlotId);
        }

        public async Task Update(Appointment appointment)
        {
            _db.Appointments.Update(appointment);
            await _db.SaveChangesAsync();
        }
    }
}
