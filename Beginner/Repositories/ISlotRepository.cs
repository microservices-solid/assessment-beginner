﻿using Beginner.Entities;

namespace Beginner.Repositories
{
    public interface ISlotRepository
    {
        public bool IsSlotExist(Guid id);
        public bool IsSlotExist(Slot slot);
        public Task Add(Slot slot);
        public Task<List<Slot>> Get(Guid? id);
        public Task<List<Slot>> GetAvailableSlots();
        public Task<List<Slot>> GetByDoctorId(Guid doctorId);
        public Task<List<Slot>> GetByDoctorName(string name);
        public Task<List<Slot>> GetReservedSlotsByDoctorName(string name);
        public Task Update(Slot slot);
    }
}
