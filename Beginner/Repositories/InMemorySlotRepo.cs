﻿using Beginner.Controllers.Dtos;
using Beginner.Entities;
using Beginner.Exceptions;
using Beginner.Repositories.Exceptions;

namespace Beginner.Repositories
{
    public class InMemorySlotRepo : ISlotRepository
    {
        private static List<Doctor> Doctors = new List<Doctor>{
            new Doctor { name = "InMemDoc1", id = Guid.NewGuid() },
            new Doctor { name = "InMemDoc2", id = Guid.NewGuid() },
            new Doctor { name = "InMemDoc3", id = Guid.NewGuid() }
        };
        private static List<Slot> Slots = new List<Slot> {
            new Slot { Id = Guid.NewGuid(), DoctorId = (Guid)Doctors[0].id, DoctorName = Doctors[0].name, Time=DateTime.Now , Cost = 100, IsReserved = false },
            new Slot { Id = Guid.NewGuid(), DoctorId = (Guid)Doctors[0].id, DoctorName = Doctors[0].name, Time=DateTime.Now , Cost = 100, IsReserved = false },
            new Slot { Id = Guid.NewGuid(), DoctorId = (Guid)Doctors[1].id, DoctorName = Doctors[1].name, Time=DateTime.Now , Cost = 200, IsReserved = false },
            new Slot { Id = Guid.NewGuid(), DoctorId = (Guid)Doctors[1].id, DoctorName = Doctors[1].name, Time=DateTime.Now , Cost = 200, IsReserved = false },
            new Slot { Id = Guid.NewGuid(), DoctorId = (Guid)Doctors[2].id, DoctorName = Doctors[2].name, Time=DateTime.Now , Cost = 300, IsReserved = false },
            new Slot { Id = Guid.NewGuid(), DoctorId = (Guid)Doctors[2].id, DoctorName = Doctors[2].name, Time=DateTime.Now , Cost = 300, IsReserved = false }
        };

        public async Task Add(Slot slot)
        {
            Slots.Add(slot);
        }

        //If id provided, return slot
        //If id not provided, return list of slots
        public async Task<List<Slot>> Get(Guid? id)
        {
            if (id == Guid.Empty)
            {
                throw new SlotIdEmptyException();
            }

            if (id == null)
            {
                return Slots;
            }

            var slot = Slots.Find(x => x.Id.CompareTo(id) == 0);

            if (slot == null)
            {
                throw new SlotNotFoundException((Guid)id);
            }

            return new List<Slot> { slot };
        }

        public async Task Update(Slot slot)
        {
            var index = Slots.FindIndex(x => x.Id.CompareTo(slot.Id) == 0);
            if (index == -1)
            {
                throw new SlotNotFoundException(slot.Id);
            }

            Slots[index] = slot;
        }

        public bool IsSlotExist(Guid id)
        {
            return Slots.Any(x => x.Id.CompareTo(id) == 0);
        }

        public bool IsSlotExist(Slot s)
        {
            return Slots.Any(x => x.DoctorName == s.DoctorName && x.Time.ToString("yyyy-mm-dd hh-mm tt") == s.Time.ToString("yyyy-mm-dd hh-mm tt"));
        }

        public async Task<List<Slot>> GetByDoctorId(Guid doctorId)
        {
            if (doctorId == Guid.Empty)
            {
                throw new DoctorIdInvalidException();
            }

            var slots = Slots.FindAll(x => x.DoctorId.CompareTo(doctorId) == 0);
            return slots;
        }

        public async Task<List<Slot>> GetByDoctorName(string name)
        {
            if (name == null || name == "")
            {
                throw new DoctorNameInvalidException();
            }

            var slots = Slots.FindAll(x => x.DoctorName == name);
            return slots;
        }


        public async Task<List<Slot>> GetAvailableSlots()
        {
            var slots = Slots.FindAll(x => !x.IsReserved && x.Time > DateTime.Now);
            return slots;
        }

        public async Task<List<Slot>> GetReservedSlotsByDoctorName(string name)
        {
            if (name == null || name == "")
            {
                throw new DoctorNameInvalidException();
            }

            var slots = Slots.FindAll(x => x.DoctorName == name && x.IsReserved == true);
            return slots;
        }
    }
}
