﻿using Beginner.Entities;
using Microsoft.EntityFrameworkCore;

namespace Beginner.Database
{
    public class ApplicationDatabase : DbContext
    {
        public DbSet<Slot> Slots { get; set; }
        public DbSet<Appointment> Appointments { get; set; }

        public ApplicationDatabase(DbContextOptions<ApplicationDatabase> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("Application_Db");
            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
        }
    }

    public static class DbExtension
    {
        public static IServiceCollection AddApplicationDb(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDatabase>(options =>
            {
                options.UseNpgsql(configuration.GetConnectionString("Postgres"));
            });
            return services;
        }
    }
}
