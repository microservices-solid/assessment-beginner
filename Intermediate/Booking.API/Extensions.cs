﻿using Booking.Application.UseCases;
using Booking.Domain.Contracts;
using Booking.Infrastructure.Database;
using Booking.Infrastructure.Repositories;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Booking.API
{
    public static class Extensions
    {
        public static IServiceCollection AddBookingModule(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAppointmentDb(configuration)
                .AddTransient<IAppointmentRepository, AppointmentRepo>()
                .AddTransient<GetAllDoctorsAvailableSlots>()
                .AddTransient<BookAppointment>()
                .AddTransient<GetUpcomingAppointmentsForDoctor>()
                .AddTransient<SetAppointmentCompleted>()
                .AddTransient<DeleteAppointment>()
                .AddTransient<GetAllAppointments>();
            return services;
        }

        public static IApplicationBuilder UseBookingModule(this IApplicationBuilder app)
        {
            return app;
        }
    }
}