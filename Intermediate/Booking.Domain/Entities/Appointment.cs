﻿using Booking.Domain.Exceptions;
using System.ComponentModel.DataAnnotations;

namespace Booking.Domain.Entities
{
    public class Appointment
    {
        //Id should be unique
        [Required] public Guid Id { get; private set; }
        //SlotId should be unique
        [Required] public Guid SlotId { get; private set; }
        [Required] public Guid PatientId { get; private set; }
        [Required] public string PatientName { get; private set; }
        [Required] public DateTime ReservedAt { get; private set; }
        public bool IsCompleted { get; private set; }

        private Appointment(Guid id, Guid slotId, Guid patientId, string patientName, DateTime reservedAt, bool isCompleted)
        {
            Id = id;
            SlotId = slotId;
            PatientId = patientId;
            PatientName = patientName;
            ReservedAt = reservedAt;
            IsCompleted = isCompleted;
        }

        public static Appointment CreateNew(SlotId slotId, PatientId patientId, PatientName patientName)
        {
            var appointment = new Appointment(Guid.NewGuid(), slotId._value, patientId._value, patientName._value, DateTime.Now.ToUniversalTime(), false);
            return appointment;
        }

        public void SetIsCompleted(bool isCompleted)
        {
            this.IsCompleted = isCompleted;
        }

        
    }

    public record SlotId
    {
        public Guid _value { get; private set; }

        public SlotId(Guid value)
        {
            if (value == Guid.Empty)
            {
                throw new SlotIdEmptyException();
            }

            _value = value;
        }
    }

    public record PatientId
    {
        public Guid _value { get; private set; }

        public PatientId(Guid value)
        {
            if (value == Guid.Empty)
            {
                throw new PatientIdEmptyException();
            }
            _value = value;
        }
    }

    public record PatientName
    {
        public string _value { get; private set; }

        public PatientName(string value)
        {
            if (value == null || value == "")
            {
                throw new PatientNameInvalidException();
            }

            _value = value;
        }
    }
}
