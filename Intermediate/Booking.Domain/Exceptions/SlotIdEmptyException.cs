﻿using System.Runtime.Serialization;

namespace Booking.Domain.Exceptions
{
    [Serializable]
    internal class SlotIdEmptyException : Exception
    {
        public SlotIdEmptyException()
        {
        }

        public SlotIdEmptyException(string? message) : base(message)
        {
        }

        public SlotIdEmptyException(string? message, Exception? innerException) : base(message, innerException)
        {
        }

        protected SlotIdEmptyException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}