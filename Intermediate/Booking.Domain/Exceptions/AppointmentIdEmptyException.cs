﻿namespace Booking.Domain.Exceptions
{
    [Serializable]
    public class AppointmentIdEmptyException : Exception
    {
        public AppointmentIdEmptyException() : base("Appointment Id should not be empty!")
        {
        }
    }
}