using Management.API;
using Booking.API;
using Authentication.API;
using Microsoft.AspNetCore.HttpLogging;
using Serilog;
using Notification.API;

var builder = WebApplication.CreateBuilder(args);
builder.Host.UseSerilog((context, services, configuration) =>
{
    configuration
        .ReadFrom.Configuration(context.Configuration)
        .ReadFrom.Services(services);
});
builder.Services.AddHttpLogging(options =>
{
    options.LoggingFields = HttpLoggingFields.All;
});
builder.Services.AddAuthenticationModule(builder.Configuration);
builder.Services.AddNotificationModule();
builder.Services.AddManagementModule(builder.Configuration);
builder.Services.AddBookingModule(builder.Configuration);
builder.Services.AddControllers();
var app = builder.Build();

app.UseHttpLogging();
app.MapGet("/", () => "Appointment Booking!");
app.MapControllers();
app.Logger.LogInformation("Appointment Booking Application started!");
app.Run();

namespace AppointmentBooking.Bootstrapper
{
    public partial class Program { }
}