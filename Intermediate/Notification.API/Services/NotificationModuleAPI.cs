﻿using Booking.Application.Dtos;
using Notification.Application;
using Notification.Shared;

namespace Notification.API.Services
{
    public class NotificationModuleAPI : INotificationModuleAPI
    {
        private readonly SendConfirmationNotification _sendConfirmationNotification;

        public NotificationModuleAPI(SendConfirmationNotification sendConfirmationNotification) { 
            _sendConfirmationNotification = sendConfirmationNotification;
        }

        public async Task SendConfirmation(NewAppointmentRequest request)
        {
            await _sendConfirmationNotification.Execute(request);
        }
    }
}
