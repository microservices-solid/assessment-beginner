﻿using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Notification.API.Services;
using Notification.Application;
using Notification.Shared;

namespace Notification.API
{
    public static class Extensions
    {
        public static IServiceCollection AddNotificationModule(this IServiceCollection services)
        {
            services.AddTransient<INotificationModuleAPI, NotificationModuleAPI>();
            services.AddTransient<SendConfirmationNotification>();
            return services;
        }

        public static IApplicationBuilder UseNotificationModule(this IApplicationBuilder app)
        {
            return app;
        }
    }
}