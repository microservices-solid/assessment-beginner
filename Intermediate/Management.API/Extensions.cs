﻿using Management.API.Services;
using Management.Application.Services;
using Management.Domain.Contracts;
using Management.Infrastructure.Database;
using Management.Infrastructure.Repositories;
using Management.Shared;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Management.API
{
    public static class Extensions
    {
        public static IServiceCollection AddManagementModule(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddSlotDb(configuration)
                .AddTransient<ISlotRepository, SlotRepo>()
                .AddTransient<ISlotService, SlotService>()
                .AddTransient<IManagementModuleAPI, ManagementModuleAPI>();
            return services;
        }

        public static IApplicationBuilder UseManagementModule(this IApplicationBuilder app)
        {
            return app;
        }
    }
}