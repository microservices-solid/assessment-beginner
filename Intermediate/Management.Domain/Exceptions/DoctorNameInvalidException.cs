﻿using System.Runtime.Serialization;

namespace Management.Domain.Exceptions
{
    [Serializable]
    public class DoctorNameInvalidException : Exception
    {
        public DoctorNameInvalidException() : base("Doctor name is invalid!")
        {
        }
    }
}