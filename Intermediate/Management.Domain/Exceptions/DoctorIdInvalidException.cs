﻿namespace Management.Domain.Exceptions
{
    public class DoctorIdInvalidException : Exception
    {
        public DoctorIdInvalidException() : base("Doctor Id is invalid!")
        {
        }
    }
}
