﻿namespace Booking.Application.Exceptions
{
    public class SlotNotFoundException : Exception
    {
        public SlotNotFoundException() : base($"Slot not found!")
        {

        }

        public SlotNotFoundException(Guid id) : base($"Slot with id {id} not found!")
        {

        }
    }
}
