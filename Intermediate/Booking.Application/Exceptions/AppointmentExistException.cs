﻿namespace Booking.Application.Exceptions
{
    [Serializable]
    internal class AppointmentExistException : Exception
    {
        public AppointmentExistException(Guid id) : base($"Appointment id {id} already exist!")
        {
        }
    }
}