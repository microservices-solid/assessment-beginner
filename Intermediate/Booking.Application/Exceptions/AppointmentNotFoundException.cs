﻿namespace Booking.Application.Exceptions
{
    [Serializable]
    internal class AppointmentNotFoundException : Exception
    {
        public AppointmentNotFoundException() : base($"Appointment not found!")
        {

        }

        public AppointmentNotFoundException(Guid id) : base($"Appointment with id {id} not found!")
        {

        }
    }
}