﻿using Booking.Application.Exceptions;
using Booking.Domain.Contracts;
using Management.Shared;

namespace Booking.Application.UseCases
{
    public class DeleteAppointment
    {
        private IAppointmentRepository _appointmentRepository;
        private IManagementModuleAPI _managementModuleAPI;
        public DeleteAppointment(IAppointmentRepository appointmentRepository, IManagementModuleAPI managementModuleAPI) {
            _appointmentRepository = appointmentRepository;
            _managementModuleAPI = managementModuleAPI;
        }

        public async Task Execute(Guid id)
        {
            //Check if appointment exist
            var appointment = await _appointmentRepository.GetByAppointmentId(id);
            if (appointment == null)
            {
                throw new AppointmentNotFoundException();
            }

            //Check current datetime with appointment datetime
            //if passed appointment time, delete the appointment
            //if not passed appointement time, delete appointment and mark slot as not reserved                
            var slot = await _managementModuleAPI.GetBySlotId(appointment.SlotId);
            if (slot == null)
            {
                throw new SlotNotFoundException();
            }

            await _appointmentRepository.Delete(id);

            if (DateTime.Now.ToUniversalTime() < slot.Time)
            {
                await _managementModuleAPI.SetSlotIsReserved(slot, false);
            }
        }
    }
}
