﻿using Booking.Domain.Contracts;
using Booking.Domain.Entities;

namespace Booking.Application.UseCases
{    
    public class GetAllAppointments
    {
        private readonly IAppointmentRepository _appointmentRepository;
        public GetAllAppointments(IAppointmentRepository appointmentRepository) {
            _appointmentRepository = appointmentRepository;
        }

        public async Task<List<Appointment>> Execute()
        {
            return await _appointmentRepository.Get(null);
        }
    }
}
