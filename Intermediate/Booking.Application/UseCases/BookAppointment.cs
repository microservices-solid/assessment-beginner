﻿using Booking.Application.Dtos;
using Booking.Application.Exceptions;
using Booking.Domain.Contracts;
using Booking.Domain.Entities;
using Management.Shared;

namespace Booking.Application.UseCases
{
    public class BookAppointment
    {
        private readonly IManagementModuleAPI _managementModuleAPI;
        private readonly IAppointmentRepository _appointmentRepository;
        public BookAppointment(IManagementModuleAPI managementModuleAPI, IAppointmentRepository appointmentRepository) { 
            _managementModuleAPI = managementModuleAPI;
            _appointmentRepository = appointmentRepository;
        }

        public async Task Execute(NewAppointmentRequest request)
        {
            //Check if slot exist
            var slot = await _managementModuleAPI.GetBySlotId(request.SlotId);
            if (slot == null)
            {
                throw new SlotNotFoundException(request.SlotId);
            }

            //Check if slot DateTime already expired
            if (DateTime.Now.ToUniversalTime() > slot.Time)
            {
                throw new SlotTimeExpiredException();
            }

            Appointment appointment = Appointment.CreateNew(new SlotId(request.SlotId), new PatientId (request.PatientId), new PatientName (request.PatientName));
            await _appointmentRepository.Add(appointment);

            //Need to update isReserved property of Slot to reserved before returning Ok
            await _managementModuleAPI.SetSlotIsReserved(slot, true);
        }
    }
}
