﻿using Booking.Application.Exceptions;
using Booking.Domain.Contracts;

namespace Booking.Application.UseCases
{
    public class SetAppointmentCompleted
    {
        private IAppointmentRepository _appointmentRepository;
        public SetAppointmentCompleted(IAppointmentRepository appointmentRepository) { 
            _appointmentRepository = appointmentRepository;
        }

        public async Task Execute(Guid id)
        {
            //Check if appointment exist
            var appointment = await _appointmentRepository.GetByAppointmentId(id);
            if (appointment == null)
            {
                throw new AppointmentNotFoundException();
            }

            await _appointmentRepository.SetAppointmentIsCompleted(id, true);
        }
    }
}
