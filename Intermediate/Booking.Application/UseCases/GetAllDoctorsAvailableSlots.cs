﻿using Management.Shared;

namespace Booking.Application.UseCases
{
    public class GetAllDoctorsAvailableSlots
    {
        private readonly IManagementModuleAPI _managementModuleAPI;

        public GetAllDoctorsAvailableSlots(IManagementModuleAPI managementModuleAPI)
        {
            _managementModuleAPI = managementModuleAPI;
        }

        public async Task<List<SlotDto>> Execute()
        {
            return await _managementModuleAPI.GetAvailableSlots();
        }
    }
}
