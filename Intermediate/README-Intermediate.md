﻿# Intermediate Microservices Training

## Description:

You are tasked The objective of this assessment project is to build upon the existing Doctor
Appointment Booking System and incorporate additional concepts such as unit and integration
testing, authorization, logging, and modular monolith architecture.

## Requirements:

Enhance the Doctor Appointment Booking System by incorporating the following additional
features/concepts:


## Appointment Confirmation:

- Once a patient schedules an appointment, the system should send a confirmation
notification to both the patient and the doctor.
- The confirmation notification should include the appointment details, such as the
patient's name, appointment time, and Doctor's name.
- For the sake of this assessment, the notification could be just a Log message

### API
```powershell
### Create an appointment
POST /appointments/slots
Content-Type: application/json

{
  "slotId": "ed11f995-66ec-4a2a-a5dd-e962a9c969b3",
  "patientId": "c45aca88-c500-45a5-81c9-811472cd938e",
  "patientName": "InMemPatient1"
}
```


## Authentication and Authorization:

- Implement an authentication mechanism to secure the application's APIs.
- Only authorized users should be able to access the appointment booking and
management functionality.

### API

- Implement an authentication mechanism to secure the application's APIs:

```powershell
### 
```

## Logging:

- Enhance the logging functionality to capture detailed information about the
system's activities.
- Utilize the Serilog logging framework to log in to the console as well as in seq
- Include log messages for significant events, errors, and important user actions.


## Modular Monolith Architecture:

- Refactor the existing system into a modular monolith architecture
- The system should consist of four modules each with diff architecture as follows:
    - Booking Module: to handle patient booking (Clean architecture and DDD)
    - Notification Module: to send notifications once an appointment is scheduled (Simplest architecture possible)
    - Management Module: to manage doctor slots (Traditional Layered Architecture)
         > Use API, Application, Domain and Infrastructure layers
    - Authentication Module: to handle authentication and authorization (Simplest architecture possible)


## Deliverables:

Modify and push to the same GitHub/Gitlab repo you used
Ensure that your commits are done incrementally and reflect your progress throughout the
development process.
Make regular and meaningful commitments to showcase your implementation approach and
demonstrate how you have broken down the project into smaller steps.


## Evaluation Criteria:

Your project will be evaluated based on the following criteria:
1. Correct implementation of all the required business requirements.
2. Proper use of relevant technologies, such as REST APIs and EF Core.
3. Code quality, including readability, maintainability, and adherence to best practices.
4. Effective implementation of the business logic and error handling.