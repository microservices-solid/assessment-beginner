﻿using Booking.Application.Dtos;
using Management.Shared;
using Microsoft.Extensions.Logging;

namespace Notification.Application
{
    public class SendConfirmationNotification
    {
        private ILogger<SendConfirmationNotification> _logger;
        private readonly IManagementModuleAPI _managementModuleAPI;

        public SendConfirmationNotification(IManagementModuleAPI managementModuleAPI, ILogger<SendConfirmationNotification> logger) { 
            _managementModuleAPI = managementModuleAPI;
            _logger = logger;
        }

        public async Task Execute (NewAppointmentRequest request)
        {
            var slot = await _managementModuleAPI.GetBySlotId(request.SlotId);
            _logger.LogInformation($"Appointment at UTC time {slot.Time.ToUniversalTime()} for doctor {slot.DoctorName} booked by patient {request.PatientName}");
        }
    }
}