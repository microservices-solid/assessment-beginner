﻿using Management.Domain.Contracts;
using Management.Domain.Entities;
using Management.Domain.Exceptions;
using Management.Infrastructure.Exceptions;

namespace Management.Infrastructure.Repositories
{
    public class InMemorySlotRepo : ISlotRepository
    {
        private static List<Doctor> Doctors = new List<Doctor>{
            new Doctor { Name = "InMemDoc1", Id = Guid.NewGuid() },
            new Doctor { Name = "InMemDoc2", Id = Guid.NewGuid() },
            new Doctor { Name = "InMemDoc3", Id = Guid.NewGuid() }
        };
        private static List<Slot> Slots = new List<Slot> {
            new Slot { Id = Guid.NewGuid(), DoctorId = Doctors[0].Id, DoctorName = Doctors[0].Name, Time=DateTime.Now , Cost = 100, IsReserved = false },
            new Slot { Id = Guid.NewGuid(), DoctorId = Doctors[0].Id, DoctorName = Doctors[0].Name, Time=DateTime.Now , Cost = 100, IsReserved = false },
            new Slot { Id = Guid.NewGuid(), DoctorId = Doctors[1].Id, DoctorName = Doctors[1].Name, Time=DateTime.Now , Cost = 200, IsReserved = false },
            new Slot { Id = Guid.NewGuid(), DoctorId = Doctors[1].Id, DoctorName = Doctors[1].Name, Time=DateTime.Now , Cost = 200, IsReserved = false },
            new Slot { Id = Guid.NewGuid(), DoctorId = Doctors[2].Id, DoctorName = Doctors[2].Name, Time=DateTime.Now , Cost = 300, IsReserved = false },
            new Slot { Id = Guid.NewGuid(), DoctorId = Doctors[2].Id, DoctorName = Doctors[2].Name, Time=DateTime.Now , Cost = 300, IsReserved = false }
        };

        public async Task Add(Slot slot)
        {
            Slots.Add(slot);
        }

        //If id provided, return slot
        //If id not provided, return list of slots
        public async Task<List<Slot>> Get(Guid? id)
        {
            if (id == Guid.Empty)
            {
                throw new SlotIdEmptyException();
            }

            if (id == null)
            {
                return Slots;
            }

            var slot = Slots.Find(x => x.Id.CompareTo(id) == 0);

            if (slot == null)
            {
                throw new SlotNotFoundException((Guid)id);
            }

            return new List<Slot> { slot };
        }

        public async Task<Slot?> GetBySlotId(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new SlotIdEmptyException();
            }

            var slot = Slots.Find(x => x.Id.CompareTo(id) == 0);

            if (slot == null)
            {
                throw new SlotNotFoundException(id);
            }

            return slot;
        }

        public async Task Update(Slot slot)
        {
            var index = Slots.FindIndex(x => x.Id.CompareTo(slot.Id) == 0);
            if (index == -1)
            {
                throw new SlotNotFoundException(slot.Id);
            }

            Slots[index] = slot;
        }

        public bool IsSlotExist(Guid id)
        {
            return Slots.Any(x => x.Id.CompareTo(id) == 0);
        }

        public bool IsSlotExist(Slot s)
        {
            return Slots.Any(x => x.DoctorName == s.DoctorName && x.Time.ToString("yyyy-mm-dd hh-mm tt") == s.Time.ToString("yyyy-mm-dd hh-mm tt"));
        }

        public async Task<List<Slot>> GetByDoctorId(Guid doctorId)
        {
            if (doctorId == Guid.Empty)
            {
                throw new DoctorIdInvalidException();
            }

            var slots = Slots.FindAll(x => x.DoctorId.CompareTo(doctorId) == 0);
            return slots;
        }

        public async Task<List<Slot>> GetByDoctorName(string name)
        {
            if (name == null || name == "")
            {
                throw new DoctorNameInvalidException();
            }

            var slots = Slots.FindAll(x => x.DoctorName == name);
            return slots;
        }


        public async Task<List<Slot>> GetAvailableSlots()
        {
            var slots = Slots.FindAll(x => !x.IsReserved && x.Time > DateTime.Now);
            return slots;
        }

        public async Task<List<Slot>> GetReservedSlotsByDoctorName(string name)
        {
            if (name == null || name == "")
            {
                throw new DoctorNameInvalidException();
            }

            var slots = Slots.FindAll(x => x.DoctorName == name && x.IsReserved == true);
            return slots;
        }

        public async Task SetSlotIsReserved(Guid id, bool isReserved)
        {
            var index = Slots.FindIndex(x => x.Id.CompareTo(id) == 0);
            if (index == -1)
            {
                throw new SlotNotFoundException(id);
            }

            Slots[index].IsReserved = isReserved;
        }

        public async Task<List<Slot>> GetReservedSlotsByDoctorId(Guid doctorId)
        {
            if (doctorId == Guid.Empty)
            {
                throw new DoctorIdInvalidException();
            }

            var slots = Slots.FindAll(x => x.DoctorId.CompareTo(doctorId) == 0 && x.IsReserved == true);
            return slots;
        }
    }
}
