﻿using Booking.Application.Dtos;

namespace Notification.Shared
{
    public interface INotificationModuleAPI
    {
        public Task SendConfirmation(NewAppointmentRequest request);
    }
}