﻿using Booking.Domain.Contracts;
using Booking.Domain.Entities;
using Booking.Domain.Exceptions;
using Booking.Infrastructure.Exceptions;

namespace Booking.Infrastructure.Repositories
{
    public class InMemoryAppointmentRepo : IAppointmentRepository
    {
        private static List<Appointment> Appointments = new List<Appointment>();


        public async Task Add(Appointment appointment)
        {
            Appointments.Add(appointment);
        }

        public async Task Delete(Guid id)
        {
            var index = Appointments.FindIndex(x => x.Id.CompareTo(id) == 0);
            if (index == -1)
            {
                throw new AppointmentNotFoundException(id);
            }

            Appointments.RemoveAt(index);
        }

        //If id provided, return appointment
        //If id not provided, return list of appointments
        public async Task<List<Appointment>> Get(Guid? id)
        {
            if (id == Guid.Empty)
            {
                throw new AppointmentIdEmptyException();
            }

            if (id == null)
            {
                return Appointments;
            }

            var appointment = Appointments.Find(x => x.Id.CompareTo(id) == 0);

            if (appointment == null)
            {
                throw new AppointmentNotFoundException((Guid)id);
            }

            return new List<Appointment> { appointment };
        }

        public async Task<List<Appointment>> GetNotCompletedAppointments()
        {
            var appointments = Appointments.FindAll(x => !x.IsCompleted);
            return appointments;
        }

        public async Task<Appointment> GetNotCompletedAppointmentBySlotId(Guid slotId)
        {
            var appointment = Appointments.Find(x => x.SlotId == slotId && x.IsCompleted == false);

            if (appointment == null)
            {
                throw new AppointmentNotFoundException();
            }

            return appointment;
        }

        public bool IsAppointmentExist(Appointment a)
        {
            return Appointments.Any(x => x.SlotId == a.SlotId);
        }

        public async Task Update(Appointment appointment)
        {
            var index = Appointments.FindIndex(x => x.Id.CompareTo(appointment.Id) == 0);
            if (index == -1)
            {
                throw new AppointmentNotFoundException(appointment.Id);
            }

            Appointments[index] = appointment;
        }

        public async Task<Appointment?> GetByAppointmentId(Guid id)
        {
            if (id == Guid.Empty)
            {
                throw new AppointmentIdEmptyException();
            }

            var appointment = Appointments.Find(x => x.Id.CompareTo(id) == 0);

            if (appointment == null)
            {
                throw new AppointmentNotFoundException((Guid)id);
            }

            return appointment;
        }

        public async Task SetAppointmentIsCompleted(Guid id, bool isCompleted)
        {
            var index = Appointments.FindIndex(x => x.Id.CompareTo(id) == 0);
            if (index == -1)
            {
                throw new AppointmentNotFoundException(id);
            }

            Appointments[index].SetIsCompleted(isCompleted);
        }
    }
}
