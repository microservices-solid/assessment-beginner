using AutoFixture;
using System.Net.Mime;
using System.Net;
using System.Text;
using Authentication.API.Dtos;
using System.Text.Json;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.Extensions.Configuration;
using System.Security.Claims;

namespace Authentication.API.Tests
{
    public class IntegrationTest : IClassFixture<CustomWebApplicationFactory>
    {
        private readonly HttpClient _client;

        private readonly IFixture _fixture = new Fixture();

        private readonly IConfiguration _configuration;
        public IntegrationTest(CustomWebApplicationFactory factory)
        {
            _client = factory.CreateClient();
            _configuration = factory._configuration;
        }

        [Fact]
        public async Task Login_POST_LoginRequestUserRoleDoctor_ShouldReturnTokenStringWithDoctorRole()
        {
            //Arrange
            var userRole = "doctor";

            await AssertUserRole(userRole);
        }


        [Fact]
        public async Task Login_POST_LoginRequestUserRolePatient_ShouldReturnTokenStringWithPatientRole()
        {
            //Arrange
            var userRole = "patient";

            await AssertUserRole(userRole);
        }

        [Fact]
        public async Task Login_POST_LoginRequestEmptyUserRole_ShouldReturn401UnauthorizedResponse()
        {
            //Arrange
            var loginRequest = new LoginRequest { UserRole = "" };
            var loginRequestPayload = new StringContent(
               JsonSerializer.Serialize(loginRequest),
               Encoding.UTF8,
               MediaTypeNames.Application.Json);

            //Act
            var response = await _client.PostAsync("/login", loginRequestPayload);

            //Assert
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        private async Task AssertUserRole(string userRole)
        {
            var loginRequest = new LoginRequest { UserRole = userRole };
            var loginRequestPayload = new StringContent(
               JsonSerializer.Serialize(loginRequest),
               Encoding.UTF8,
               MediaTypeNames.Application.Json);

            //Act
            var response = await _client.PostAsync("/login", loginRequestPayload);

            //Assert
            Assert.Equal(HttpStatusCode.OK, response.StatusCode);

            string jsonString = await response.Content.ReadAsStringAsync();
            var loginResponse = JsonSerializer.Deserialize<LoginResponse>(jsonString);
            var handler = new JwtSecurityTokenHandler();
            var token = handler.ReadJwtToken(loginResponse.token);

            Assert.Equal(userRole, token.Claims.ElementAt<Claim>(0).Value);
        }

    }
}