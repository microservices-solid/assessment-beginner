﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.Extensions.Configuration;

namespace Authentication.API.Tests
{
    public class CustomWebApplicationFactory : WebApplicationFactory<AppointmentBooking.Bootstrapper.Program>
    {
        public IConfiguration _configuration;
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            base.ConfigureWebHost(builder);
            builder.UseEnvironment("Test");
            builder.ConfigureAppConfiguration((context, conf) =>
            {
                _configuration = conf.Build();
            });
        }
    }
}
