﻿using Management.Application.Dtos;
using Management.Application.Exceptions;
using Management.Domain.Contracts;
using Management.Domain.Entities;
using Management.Domain.Exceptions;

namespace Management.Application.Services
{
    public class SlotService : ISlotService
    {
        private readonly ISlotRepository _slotRepository;

        public SlotService(ISlotRepository slotRepository)
        {
            _slotRepository = slotRepository;
        }

        //Only doctors can add
        public async Task Add(NewSlotRequest request)
        {
            //Reject request if request time is earlier than current time
            if (DateTime.Parse(request.Time) < DateTime.Now)
            {
                throw new SlotInvalidTimeException();
            }

            Slot slot = new Slot { Id = Guid.NewGuid(), Time = DateTime.Parse(request.Time).ToUniversalTime(), DoctorId = request.DoctorId, DoctorName = request.DoctorName, Cost = request.Cost, IsReserved = false };
            var exist = _slotRepository.IsSlotExist(slot);
            if (exist)
            {
                throw new SlotExistException(slot.Id);
            }

            await _slotRepository.Add(slot);
        }

        public async Task<List<Slot>> GetAllSlots()
        {
            return await _slotRepository.Get(null);
        }

        public async Task<List<Slot>> GetByDoctorId(Guid doctorId)
        {
            if (doctorId == Guid.Empty)
            {
                throw new DoctorIdInvalidException();
            }
            return await _slotRepository.GetByDoctorId(doctorId);
        }

        public async Task<List<Slot>> GetAvailableSlots()
        {
            return await _slotRepository.GetAvailableSlots();
        }

        public async Task<List<Slot>> GetReservedSlotsByDoctorName(string name)
        {
            if (name == null || name == "")
            {
                throw new DoctorNameInvalidException();
            }
            return await _slotRepository.GetReservedSlotsByDoctorName(name);
        }

        public async Task<Slot?> GetBySlotId(Guid id)
        {
            return await _slotRepository.GetBySlotId(id);
        }

        public async Task SetSlotIsReserved(Guid id, bool isReserved)
        {
            await _slotRepository.SetSlotIsReserved(id, isReserved);
        }

        public async Task<List<Slot>> GetReservedSlotsByDoctorId(Guid doctorId)
        {
            return await _slotRepository.GetReservedSlotsByDoctorId(doctorId);
        }
    }
}
